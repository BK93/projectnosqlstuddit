const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    userName: {
        type: String,
        unique: true,
        required: [true, 'userName is required.'],
        validate: {
            validator: (userName) => userName.length > 2,
            message: 'Username must be longer than 2 characters!'
        }
    },
    password: {
        type: String,
        required: [true, 'password is required.'],
        validate: {
            validator: (password) => password.length > 5,
            message: 'Password must be at least 6 characters!'
        }
    },
    threads: [{
        type: Schema.Types.ObjectId,
        ref: 'thread'
    }]
});

const User = mongoose.model('user', UserSchema);

module.exports = User;
