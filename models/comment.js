const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommentSchema = new Schema({
    userName: {
        type: String,
        ref: 'user',
        required: [true, 'User of comment is required.']
    },
    content: {
        type: String,
        required: [true, 'Content is required.']
    },
    thread: {
      type: Schema.Types.ObjectId,
      ref: 'thread'
    },
    comments: [{
        type: Schema.Types.ObjectId,
        ref: 'comment'
    }],
    upVotes: [{
        'userName': String,
    }],
    downVotes: [{
        'userName': String,
    }],
});

const Comment = mongoose.model('comment', CommentSchema);

module.exports = Comment;
