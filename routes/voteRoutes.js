const VoteController = require('../controllers/voteController');

module.exports = (app) => {

    app.post('/api/thread/:id/voteup', VoteController.upVoteThread);
    app.post('/api/thread/:id/votedown', VoteController.downVoteThread);

    app.post('/api/comment/:id/voteup', VoteController.upVoteComment);
    app.post('/api/comment/:id/votedown', VoteController.downVoteComment);
};
