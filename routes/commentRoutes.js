const CommentController = require('../controllers/commentController');

module.exports = (app) => {

    app.post('/api/thread/:id/comment', CommentController.commentToThread);
    app.delete('/api/comment/:id', CommentController.deleteComment);
    app.post('/api/comment/:id', CommentController.commentOnComment)
};
