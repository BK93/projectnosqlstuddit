const UserController = require('../controllers/userController');

module.exports = (app) => {

    app.get('/api/users', UserController.getAllUsers);
    app.post('/api/user/', UserController.createUser);
    app.put('/api/user/', UserController.changePassword);
    app.delete('/api/user/', UserController.deleteUser);
};
