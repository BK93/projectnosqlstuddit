const ThreadController = require('../controllers/threadController');

module.exports = (app) => {

    app.get('/api/threads', ThreadController.getAllThreads);
    app.get('/api/thread/:id', ThreadController.getThreadById);
    app.post('/api/thread/', ThreadController.createThread);
    app.put('/api/thread/:id', ThreadController.updateThread);
    app.delete('/api/thread/:id', ThreadController.deleteThread);
};
