const FriendshipController = require('../controllers/friendshipController');

module.exports = (app) => {

    app.post('/api/users/addfriend', FriendshipController.addFriendship);
    app.delete('/api/users/deletefriend', FriendshipController.deleteFriendship);
};
