const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const requester = require('../test/requester_test');
const expect = chai.expect;

chai.should();
chai.use(chaiHttp);

const User = mongoose.model('user');
const Thread = mongoose.model('thread');
const Comment = mongoose.model('comment');

describe('CRUD tests on comments', () => {
    let testUser;
    let testThread;

    beforeEach(async function () {
        testUser = await new User({
            'userName': 'CommentTestUser',
            'password': 'CommentTestPassword'
        }).save();

        testThread = await new Thread({
            'title': 'TestThread',
            'content': 'TestContent',
            'author': testUser
        }).save();
    });

    describe('Create test for comments', () => {
        it('(POST /thread/:id/comment) should create a new comment on a thread', async function () {
            // Arrange: declare the body of the request to be sent
            const testBody = {
                'userName': testUser.userName,
                'content': "Awesome thread!"
            };
            // Act: send request
            const response = await requester.post(`/api/thread/${testThread.id}/comment`).send(testBody);
            // Assert
            expect(response).to.have.status(200);

            const thread = await Thread.findById(testThread.id);
            expect(thread).to.have.property('title', testThread.title);
            expect(thread).to.have.property('comments').of.length(1);

            const comment = await Comment.findById(thread.comments[0]);
            expect(comment).to.have.property('content', testBody.content);
            expect(comment).to.have.property('userName', testBody.userName);
        });

        it('(POST /thread/:id/comment) should return 204 status code if threadId not found', async function () {
            const testBody = {
                'userName': testUser.userName,
                'content': "Awesome thread!"
            };

            const id = 'abcdefghijklmnopqrstuvwxefasdfwrgreheagragqrgreagfdft64534';
            const response = await requester.post(`/api/thread/${id}/comment`).send(testBody);
            expect(response).to.have.status(204);
        });

        it('(POST /comment/:id) should create a new comment on a comment', async function () {
            const testComment = await new Comment({
                'userName': testUser.userName,
                'content': "Awesome thread Jasper!"
            }).save();

            const testBody = {
                'userName': testUser.userName,
                'content': "Awesome comment Brian!"
            };
            const response = await requester.post(`/api/comment/${testComment.id}`).send(testBody);
            expect(response).to.have.status(200);

            const comment = await Comment.findById(testComment.id);
            expect(comment).to.have.property('content', testComment.content);
            expect(comment).to.have.property('comments').of.length(1);

            const commentOnComment = await Comment.findById(comment.comments[0]);
            expect(commentOnComment).to.have.property('content', testBody.content);
            expect(commentOnComment).to.have.property('userName', testBody.userName);
        });

        it('(POST /comment/:id) should return 204 status code if commentId not found', async function () {
            const testBody = {
                'userName': testUser.userName,
                'content': "Awesome comment!"
            };

            const id = 'abcdefghijklmnopqrstuvwx';
            const response = await requester.post(`/api/comment/${id}`).send(testBody);
            expect(response).to.have.status(204);
        });
    });

    describe('Delete test comment', () => {
        let testComment;

        beforeEach(async function () {
            testComment = await new Comment({
                'userName': testUser.userName,
                'content': "Awesome thread!"
            }).save();
        });

        it('(DELETE /comment/:id) should delete a comment', async function () {
            const testBody = {
                'userName': testUser.userName
            };
            const response = await requester.delete(`/api/comment/${testComment.id}`).send(testBody);
            expect(response).to.have.status(200);
        });

        it('(DELETE /comment/:id) should return 204 status code if commentId not found', async function () {
            const testBody = {
                'userName': testUser.userName
            };

            const id = 'abcdefghijklmnopqrs44534gaergreg53445hgrsdfaghrtuvwx';
            const response = await requester.delete(`/api/comment/${id}`).send(testBody);
            expect(response).to.have.status(204);
        });

        it('(DELETE /comment/:id) should return 201 status code if not the author', async function () {
            const otherUser = await new User({
                'userName': 'OtherUser',
                'password': 'Secret123'
            }).save();

            const testBody = {
                'userName': otherUser.userName
            };

            const response = await requester.delete(`/api/comment/${testComment.id}`).send(testBody);
            expect(response).to.have.status(201);
        });
    });
});
