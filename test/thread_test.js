const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const expect = chai.expect;
const requester = require('../test/requester_test');

chai.should();
chai.use(chaiHttp);

const User = mongoose.model('user');
const Thread = mongoose.model('thread');

describe('CRUD tests on threads', () => {
    let testUser;

    beforeEach(async function () {
        testUser = await new User({
            'userName': 'TestUser1',
            'password': 'TestUser1'
        }).save()
    });

    describe('Create a new thread', function () {
        it('(POST /api/thread) Should create a new thread', async function () {
            // Arrange: declare the thread
            const testThread = {
                'author': testUser.userName,
                'title': "ThisIsAThread1",
                'content': "ThreadContent1"
            };

            //Act: send request
            const response = await requester.post('/api/thread').send(testThread);

            // Assert
            expect(response).to.have.status(200);

            const thread = await Thread.findOne({ title: testThread.title });
            expect(thread).to.have.property('title', testThread.title);
        });

        it('(POST /api/thread) Should NOT create a new thread with a invalid user', async function () {
            // Arrange: declare the thread
            const invalidUserThread = {
                'author': "testUser123456789",
                'content': "TestContent",
                'title': "TestThread"
            };

            //Act: send request
            const response = await requester.post('/api/thread').send(invalidUserThread);

            // Assert
            expect(response).to.have.status(204);
        });
    });


    describe('Get all threads', function () {
        let testThread1;
        let testThread2;
        let testThread3;

        beforeEach(async function () {
            testThread1 = new Thread({
                'title': "TestThread1",
                'content': "TestContent",
                'author': testUser
            });
            testThread2 = new Thread({
                'title': "TestThread2",
                'content': "TestContent",
                'author': testUser
            });
            testThread3 = new Thread({
                'title': "TestThread3",
                'content': "TestContent",
                'author': testUser
            });
            await Promise.all([testThread1.save(), testThread2.save(), testThread3.save()]);
        });

        it('(GET /api/threads) Should return all threads', async function () {
            // Act: send request
            const response = await requester.get(`/api/threads`);
            const count = await Thread.find().countDocuments();

            // Assert
            expect(response).to.have.status(200);
            expect(count).to.equal(3);
        });

        it('(GET /api/thread/:id) Should return one thread', async function () {
            // Act: send request
            const response = await requester.get(`/api/thread/${testThread2.id}`);

            // Assert
            expect(response).to.have.status(200);
        });
    });

    describe('Update a thread', function () {
        it('(PUT /api/thread/:id) Should update a thread', async function () {
            // Arrange: declare the thread
            const testThread = await new Thread({
                'title': "TestThread12345",
                'content': "TestContent",
                'author': testUser
            }).save();

            const newThread = {
                'userName': testUser.userName,
                'password': testUser.password,
                'content': "This is new content!"
            };

            //Act: send request
            const response = await requester.put(`/api/thread/${testThread.id}`).send(newThread);

            // Assert
            expect(response).to.have.status(200);

            const thread = await Thread.findOne({ title: testThread.title });
            expect(thread).to.have.property('content', newThread.content);
        });
    });

    describe('Delete a thread', function () {
        it('(DELETE /api/thread/:id) Should delete a thread', async function () {

            // Arrange: declare the thread
            const testThread = await new Thread({
                'title': "TestThread",
                'content': "TestContent",
                'author': testUser
            }).save();

            // Act: send request
            const response = await requester.delete(`/api/thread/${testThread.id}`).send(testUser);

            // Assert
            expect(response).to.have.status(200);
        });
    });
});
