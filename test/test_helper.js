const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

beforeEach((done) => {
    const {users, threads, comments} = mongoose.connection.collections;
    users.drop(() => {
        threads.drop(() => {
            comments.drop(() => {
                done()
            })
        })
    })
});
