const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const server = require('../app');
const requester = require('../test/requester_test');
const expect = chai.expect;

chai.should();
chai.use(chaiHttp);

const User = mongoose.model('user');

describe('Friendship tests for users', () => {
        beforeEach((done) => {
            const friend1 = new User({userName: 'testUser1', password: 'LovesCoffee'});
            const friend2 = new User({userName: 'testUser2', password: 'BrianIsCool'});
            friend1.save();
            friend2.save();
            done()
        });

        it('Create an new friendship', (done) => {
            chai.request(server)
                .post('/api/users/addfriend')
                .send({
                    userName: 'testUser1',
                    friendName: 'testUser2'
                })
                .end(() => {
                });
            done()
        });

        it('Delete an existing friendship', (done) => {
            chai.request(server)
                .delete('/api/users/deletefriend')
                .send({
                    userName: 'testUser1',
                    friendName: 'testUser2'
                })
                .end(() => {
                });
            done()
        });

    // describe('Add and Delete friendship', () => {
    //     let firstUser;
    //     let secondUser;
    //
    //     beforeEach(async function () {
    //         firstUser = await new User({
    //             userName: 'FirstUser',
    //             password: 'FirstPassword'
    //         }).save();
    //
    //         secondUser = await new User({
    //             userName: 'SecondUser',
    //             password: 'SecondPassword'
    //         }).save();
    //     });
    //
    //     it('(POST /api/users/addfriend) Should create friendship between two users', async function () {
    //         const makeFriends = {
    //             userName: firstUser.userName,
    //             friendName: secondUser.userName
    //         };
    //
    //         // const response = await requester.post(`/api/users/addfriend`).send(makeFriends);
    //         const response = await requester.post(`/api/users/addfriend?firstUser=${firstUser.userName}&secondUser=${secondUser.userName}`);
    //         expect(response).to.have.status(200);
    //     });
    //
    //     it('(DELETE /api/users/deletefriend) Should remove friendship between two users', async function () {
    //         const deleteFriends = {
    //             userName: firstUser.userName,
    //             friendName: secondUser.userName
    //         };
    //
    //         // const response = await requester.post(`/api/users/deletefriend`).send(deleteFriends);
    //         const response = await requester.delete(`/api/users/deletefriend?firstUser=${firstUser.userName}&secondUser=${secondUser.userName}`);
    //         expect(response).to.have.status(200);
    //     });
    // })
});
