const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require("chai-as-promised");
const requester = require('../test/requester_test');

chai.use(chaiAsPromised);
chai.should();

const User = require('../models/user');

// application specific logging
process.on('unhandledRejection', (reason, p) => {
    console.log('Unhandled Rejection at: Promise', p, 'reason:', reason);
});

describe('CRUD tests on users', () => {
    describe('Create a new user', () => {
        it('(POST /api/user) Should create a user', async function () {
            // Arrange
            const testBody = {
                'userName': 'Jasper',
                'password': 'KoffieIsLekker'
            };

            // Act
            const response = await requester.post('/api/user').send(testBody);

            // Assert
            expect(response).to.have.status(200);

            const user = await User.findOne({userName: testBody.userName});
            expect(user).to.have.property('userName', testBody.userName);
            expect(user).to.have.property('password', testBody.password);
        });

        it('(POST /api/user) Should return 409 status code if username is already taken', async function () {
            const testUser = await new User({
                'userName': 'TestUser',
                'password': 'TestPassword'
            }).save();

            const testBody = {
                'userName': 'TestUser',
                'password': 'TestPassword'
            };
            const response = await requester.post('/api/user').send(testBody);
            expect(response).to.have.status(409);
        });

        it('(POST /api/user) Should not create a User without a userName', async function () {
            const user = new User({password: 'test123'});
            await expect(user.save()).to.be.rejectedWith(Error)
        });

        it('(POST /api/user) Should not create a User without a password', async function () {
            const user = new User({userName: 'Test'});
            await expect(user.save()).to.be.rejectedWith(Error)
        });
    });

    describe('Get all users', () => {
        it('(GET /api/user) should give all users', async function () {
            const testNameA = 'Alexander';
            const testPasswordA = 'TestingAllDayLong';

            const testNameB = 'Dion';
            const testPasswordB = 'TestingAllDayLong';

            await new User({userName: testNameA, password: testPasswordA}).save();
            await new User({userName: testNameB, password: testPasswordB}).save();

            const res = await requester.get('/api/users');

            expect(res).to.have.status(200);
            const users = res.body;
            users.sort((lhs, rhs) => lhs.userName < rhs.userName ? -1 : 1);
            expect(users).to.have.length(2);
            expect(users[1]).to.have.property('userName', testNameB);
            expect(users[1]).to.have.property('password', testPasswordB);
            expect(users[0]).to.have.property('userName', testNameA);
            expect(users[0]).to.have.property('password', testPasswordA)
        });
    });

    describe('Update a user', () => {
        let testUser;

        beforeEach(async function () {
            testUser = await new User({
                'userName': 'TestUser',
                'password': 'TestPassword'
            }).save()
        });

        it('(PUT /api/user) Should update a users password', async function () {
            const testBody = {
                'userName': testUser.userName,
                'password': testUser.password,
                'newPassword': 'NewPassword'
            };
            const response = await requester.put('/api/user').send(testBody);
            expect(response).to.have.status(200);

            const user = await User.findOne({ userName: testBody.userName });
            expect(user).to.have.property('userName', testUser.userName);
            expect(user).to.have.property('password', testBody.newPassword);
        });

        it('(PUT /api/user) Should return status code 404 if currentPassword is invalid', async function () {
            const testBody = {
                'userName': testUser.userName,
                'password': 'WrongPassword',
                'newPassword': 'NewPassword'
            };
            const response = await requester.put('/api/user').send(testBody);
            expect(response).to.have.status(401);

            const user = await User.findOne({ userName: testBody.userName });
            expect(user).to.have.property('userName', testUser.userName);
            expect(user).to.have.property('password', testUser.password);
        });

        it('(PUT /api/user) Should return status code 404 if user not found', async function () {
            const testBody = {
                'userName': 'WrongUsername',
                'password': 'WrongPassword',
                'newPassword': 'NewPassword'
            };
            const response = await requester.patch('/api/user').send(testBody);
            expect(response).to.have.status(404);
        });
    });

    describe('Delete a user', () => {
        let testUser;

        beforeEach(async function () {
            testUser = await new User({
                'userName': 'TestUser',
                'password': 'TestPassword'
            }).save()
        });

        it('(DELETE /api/user) Should delete a user', async function () {
            const testBody = {
                'userName': testUser.userName,
                'password': testUser.password
            };
            const response = await requester.delete('/api/user').send(testBody);
            expect(response).to.have.status(200);

            const user = await User.findOne({ userName: testUser.userName });
            expect(user).to.be.null;
        });

        it('(DELETE /api/user) Should return status code 401 if currentPassword is invalid', async function () {
            const testBody = {
                'userName': testUser.userName,
                'password': 'WrongPassword'
            };
            const response = await requester.delete('/api/user').send(testBody);
            expect(response).to.have.status(401);
        });

        it('(DELETE /api/user) Should return status code 404 if user not found', async function () {
            const testBody = {
                'userName': 'WrongUsername',
                'password': testUser.password
            };
            const response = await requester.delete('/api/user').send(testBody);
            expect(response).to.have.status(401);
        });
    });
});
