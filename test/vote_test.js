const chai = require('chai');
const chaiHttp = require('chai-http');
const mongoose = require('mongoose');
const expect = chai.expect;
const requester = require('../test/requester_test');

chai.should();
chai.use(chaiHttp);

const User = mongoose.model('user');
const Thread = mongoose.model('thread');
const Comment = mongoose.model('comment');

describe('Vote on thread', function () {
    let testUser;
    let testThread;

    beforeEach(async function () {
        testUser = await new User({
            'userName': 'TestUser1',
            'password': 'TestUser1'
        }).save();

        testThread = await new Thread({
            'title': "TestThread",
            'content': "TestContent",
            'author': testUser
        }).save();
    });

    it('(POST /api/thread/:id/voteup) should up vote a thread', async function () {
        const testBody = {
            'userName': testUser.userName
        };
        const response = await requester.post(`/api/thread/${testThread.id}/voteup`).send(testBody);
        expect(response).to.have.status(200);

        const thread = await Thread.findById(testThread.id);
        expect(thread).to.have.property('title', testThread.title);
        expect(thread).to.have.property('upVotes').of.length(1);
    });

    it('(POST /api/thread/:id/votedown) should down vote a thread', async function () {
        const testBody = {
            'userName': testUser.userName,
        };
        const response = await requester.post(`/api/thread/${testThread.id}/votedown`).send(testBody);
        expect(response).to.have.status(200);

        const thread = await Thread.findById(testThread.id);
        expect(thread).to.have.property('title', testThread.title);
        expect(thread).to.have.property('downVotes').of.length(1);
    });

    describe('Vote on comment', () => {
        let testUser;
        let testComment;

        beforeEach(async function () {
            testUser = await new User({
                'userName': 'TestUser1',
                'password': 'TestUser1'
            }).save();

            testComment = await new Comment({
                'content': "Awesome thread!",
                'userName': testUser
            }).save();
        });

        it('(POST /api/comment/:id/voteup) should up vote a comment', async function () {
            const testBody = {
                'userName': testUser.userName,
            };
            const response = await requester.post(`/api/comment/${testComment.id}/voteup`).send(testBody);
            expect(response).to.have.status(200);

            const comment = await Comment.findById(testComment.id);
            expect(comment).to.have.property('content', testComment.content);
            expect(comment).to.have.property('upVotes').of.length(1);
        });

        it('(POST /api/comment/:id/votedown) should down vote a comment', async function () {
            const testBody = {
                'userName': testUser.userName,
            };
            const response = await requester.post(`/api/comment/${testComment.id}/votedown`).send(testBody);
            expect(response).to.have.status(200);

            const comment = await Comment.findById(testComment.id);
            expect(comment).to.have.property('content', testComment.content);
            expect(comment).to.have.property('downVotes').of.length(1);
        });
    })
});
