const Comment = require('../models/comment');
const Thread = require('../models/thread');
const User = require('../models/user');
const assert = require('assert');

module.exports = {

    upVoteThread(req, res, next) {
        const userName = req.body.userName;
        const threadId = req.params.id;
        const user = {userName: userName};

        try {
            assert(userName, 'Username must be provided!');
            assert(threadId, 'Thread id must be provided!');
        } catch(err) {
            next(res.status(422).send(err.message));
        }

        User.findOne({ userName: userName })
            .then(users => {
                if (users === null) {
                    next(res.status(204).send({"Message": 'User does not exist!'}))
                } else {
                    Thread.find({_id: threadId, upVotes: {$elemMatch: user}}, function (err, thread) {
                        if (thread.length) {
                            next(res.status(400).send({"Message": 'This user already up voted!'}));
                        } else {
                            Thread.updateMany({_id: threadId}, {$pull: {downVotes: user}}, {new: true}, function (err, thread) {
                                Thread.findByIdAndUpdate(threadId, {$push: {upVotes: user}}, {new: true}, function (err, thread) {
                                    if (err) {
                                        next(err);
                                    }
                                    next(res.status(200).send({"Message": 'Upvote added!'}));
                                });
                            });
                        }
                    });
                }
            })
            .catch(error => {
                res.status(error.status).send({ Message: error.message })
            });
    },

    downVoteThread(req, res, next) {
        const userName = req.body.userName;
        const threadId = req.params.id;
        const user = {userName: userName};

        try {
            assert(userName, 'Username must be provided!');
            assert(threadId, 'Thread id must be provided!');
        } catch(err) {
            next(res.status(422).send(err.message));
        }

        User.findOne({ userName: userName })
            .then(users => {
                if (users === null) {
                    next(res.status(204).send({"Message": 'User does not exist!'}))
                } else {
                    Thread.find({_id: threadId, downVotes: {$elemMatch: user}}, function (err, thread) {
                        if (thread.length) {
                            next(res.status(400).send({"Message": 'This user already down voted!'}));
                        } else {
                            Thread.updateMany({_id: threadId}, {$pull: {upVotes: user}}, {new: true}, function (err, thread) {
                                Thread.findByIdAndUpdate(threadId, {$push: {downVotes: user}}, {new: true}, function (err, thread) {
                                    if (err) {
                                        next(err);
                                    }
                                    next(res.status(200).send({"Message": 'Downvote added!'}));
                                });
                            });
                        }
                    })
                }
            })
            .catch(error => {
                res.status(error.status).send({ Message: error.message })
            });
    },

    upVoteComment(req, res, next) {
        const userName = req.body.userName;
        const commentId = req.params.id;
        const user = {userName: userName};

        try {
            assert(userName, 'Username must be provided!');
            assert(commentId, 'Comment id must be provided!');
        } catch(err) {
            next(res.status(422).send(err.message));
        }

        User.findOne({ userName: userName })
            .then(users => {
                if (users === null) {
                    next(res.status(204).send({"Message": 'User does not exist!'}))
                } else {
                    Comment.find({_id: commentId, upVotes: {$elemMatch: user}}, function (err, comment) {
                        if (comment.length) {
                            next(res.status(400).send({"Message": 'This user already up voted!'}));
                        } else {
                            Comment.updateMany({_id: commentId}, {$pull: {downVotes: user}}, {new: true}, function (err, comment) {
                                Comment.findByIdAndUpdate(commentId, {$push: {upVotes: user}}, {new: true}, function (err, comment) {
                                    if (err) {
                                        next(err);
                                    }
                                    next(res.status(200).send({"Message": 'Upvote added!'}));
                                });
                            });
                        }
                    })
                }
            })
            .catch(error => {
                res.status(error.status).send({ Message: error.message })
            });
    },

    downVoteComment(req, res, next) {
        let userName = req.body.userName;
        let commentId = req.params.id;
        let user = {userName: userName};

        try {
            assert(userName, 'Username must be provided!');
            assert(commentId, 'Comment id must be provided!');
        } catch (err) {
            next(res.status(422).send(err.message));
        }

        User.findOne({userName: userName})
            .then(users => {
                if (users === null) {
                    next(res.status(204).send({"Message": 'User does not exist!'}))
                } else {
                    Comment.find({_id: commentId, downVotes: {$elemMatch: user}}, function (err, comment) {
                        if (comment.length) {
                            next(res.status(400).send({"Message": 'This user already down voted!'}));
                        } else {
                            Comment.updateMany({_id: commentId}, {$pull: {upVotes: user}}, {new: true}, function (err, comment) {
                                Comment.findByIdAndUpdate(commentId, {$push: {downVotes: user}}, {new: true}, function (err, comment) {
                                    if (err) {
                                        next(err);
                                    }
                                    next(res.status(200).send({"Message": 'Downvote added!'}));
                                });
                            });
                        }
                    })
                }
            })
            .catch(error => {
                res.status(error.status).send({ Message: error.message })
            });
    }
};
