const Comment = require('../models/comment');
const Thread = require('../models/thread');
const User = require('../models/user');
const assert = require('assert');

module.exports = {

    commentToThread(req, res, next) {
        const userName = req.body.userName;
        const content = req.body.content;
        const threadId = req.params.id;

        try {
            assert(threadId, 'Thread id must be provided!');
            assert(content, 'Content must be provided!');
            assert(userName, 'Username must be provided!');
        } catch (err) {
            next(res.status(422).send(err.message));
        }

        User.findOne({ userName: userName })
            .then(user => {
                if(user === null) {
                    next(res.status(204).send({ "Message": 'User does not exist!' }))
                } else {
                    Thread.findOne({'_id': threadId})
                        .then((thread) => {
                            const comment = new Comment({
                                "userName": userName,
                                "content": content
                            });
                            thread.comments.push(comment);

                            Promise.all([thread.save(), comment.save()])
                                .then(() => {
                                    next(res.status(200).send({"Message": 'Comment on thread succesfully placed!'}))
                                })
                                .catch((err) => {
                                    next(res.status(204).send({"Message": 'Comment on thread not placed!'}))
                                });
                        })
                        .catch((error) => {
                            next(res.status(204).send({'Message': 'Thread not found!'}))
                        });
                }
            })
    },

    commentOnComment(req, res, next) {
        const userName = req.body.userName;
        const content = req.body.content;
        const commentId = req.params.id;

        try {
            assert(commentId, 'CommentId must be provided!');
            assert(userName, 'Username must be provided!');
            assert(content, 'Content must be provided!');
        } catch (err) {
            next(res.status(422).send(err.message));
        }

        User.findOne({ userName: userName })
            .then(user => {
                if (user === null) {
                    next(res.status(204).send({"Message": 'User does not exist!'}))
                } else {
                    Comment.findOne({'_id': commentId})
                        .then((comment) => {
                            const newComment = new Comment({
                                "userName": userName,
                                "content": content
                            });
                            comment.comments.push(newComment);

                            Promise.all([comment.save(), newComment.save()])
                                .then(() => {
                                    res.status(200).send({
                                        message: "Comment on comment added successfully!"
                                    });
                                })
                                .catch((err) => {
                                    next(res.status(204).send({"Message": 'Comment on comment not placed!'}))
                                });
                        })
                        .catch(() => {
                            res.status(204).send({"message": "Comment not found!"})
                        });
                }
            })
    },

    deleteComment(req, res) {
        const userName = req.body.userName;

        Comment.findOne({_id: req.params.id})
            .then((comment) => {
                if (comment === undefined) {
                    res.status(204).send({"Message": 'Comment not found!'})
                }
                if (comment.userName !== userName) {
                    res.status(201).send({"Message": 'userName does not match!'})
                } else {
                    comment.remove();
                    res.status(200).send({
                        message: "Comment deleted successfully."
                    });
                }
            })
            .catch(() => res.status(204).send({ "Message": "Comment not found!" }))
    }
};
