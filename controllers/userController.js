const User = require('../models/user');
const { session } = require('../neo4j_setup');
const assert = require('assert');
const validator = require('../helpers/validation');

module.exports = {

    getAllUsers(req, res){
        User.find({})
            .then((users) => {
                res.status(200).json(users).end();
            })
            .catch((error) => {
                res.status(500).json(error).end();
            });
    },

    createUser(req, res, next) {
        const userName = req.body.userName;
        const password = req.body.password;

        try {
            assert(userName, 'Username must be provided!');
            assert(password, 'Password must be provided!');
        } catch(err) {
           next(res.status(422).send(err.message));
        }
        const user = new User(req.body);

        validator.findUser(user.userName)
            .then((user) => {
                res.status(409).send({
                    message: `Username ${user.userName} is already taken.`
                });
            })
            .catch(() => {
                user.save()
                    .then(user => {
                        res.status(200).send({
                            message: "User successfully created!",
                            userName: user.userName,
                            password: user.password
                        });
                    })
                    .catch(error => {
                        res.status(error.status).send({ "message": error.message });
                    })
                    .then(() => {
                        session.run(
                            'CREATE (u:User {userName: $userName}) RETURN u',
                            {userName: userName})
                            .then((result) => {
                                res.status(200).send({"Message": "New User is created succesfully!"})
                            })
                            .catch((error) => {
                                // next(res.status(412).send({ "message": error.message }));
                            });
                    })
                    .catch((error) => {
                        next(res.status(412).send({ "message": error.message }));
                    })
            });
    },

    changePassword(req, res, next) {
        const userName = req.body.userName;
        const password = req.body.password;
        const newPassword = req.body.newPassword;

        try {
            assert(userName, 'username must be provided');
            assert(password, 'password must be provided');
            assert(newPassword, 'newPassword must be provided');
        } catch(err) {
            next(res.status(422).send(err.message));
        }

        validator.validateUser(userName, password)
            .then(user => {
                user.set('password', newPassword);
                user.save()
                    .then(user => {
                        res.status(200).send({
                            message: "Password successfully changed!",
                            userName: user.userName,
                            password: user.password
                        });
                    })
                    .catch(error => {
                        res.status(500).send({
                            "message": "Server error!",
                        });
                    })
            })
            .catch(error => {
                res.status(error.status).send({ "Message": error.message });
            });
    },

    deleteUser(req, res, next) {
        const userName = req.body.userName;
        const password = req.body.password;

        try {
            assert(userName, 'username must be provided');
            assert(password, 'password must be provided');
        } catch(err) {
            next(res.status(422).send({ "Message": 'One or both fields are empty!'}))
        }

        User.findOneAndDelete({ userName: userName, password: password }, (err, document) => {
            if (err) {
                next(res.status(409).send({"Message": 'Unknown error!'}))
            } else if (document === null) {
                next(res.status(401).send({"Message": 'Wrong combination of username and password or user does not exist!'}))
            } else {
                session.run('MATCH(u:User { userName: $userName }) DETACH DELETE u', {
                    userName: userName,
                    password: password
                })
                    .then((result) => {
                        res.status(200).send({"Message": 'User deleted succesfully!'})
                    })
                    .catch((err) => {
                        next(res.status(412).send({ "Message": 'Something went wrong!'}))
                    });
            }
        });
    }
};
