const Thread = require('../models/thread');
const Comment = require('../models/comment');
const User = require('../models/user');
const assert = require('assert');
const validator = require('../helpers/validation');

module.exports = {

    getAllThreads(req, res) {
        Thread.find({}, {_id: 1, title: 1, content: 1, author: 1, upVotes: 1, downVotes: 1})
            .then((thread) => {
                res.status(200).send(thread)
            })
            .catch(() => {
                res.status(204).send({ "Message": 'Threads not found!' })
            });
    },

    getThreadById(req, res) {
        const threadId = req.params.id;

        try {
            assert(threadId, 'Thread id must be provided!');
        } catch (err) {
            next(res.status(422).send(err.message));
        }

        Thread.findById(threadId)
            .populate({
                path: 'comments',
                model: 'comment',
                populate: {
                    path: 'user',
                    model: 'user'
                },
                populate: {
                    path: 'comments',
                    model: 'comment'
                }
            })
            .then((thread) => {
                res.status(200).send(thread)
            })
            .catch(() => {
                res.status(204).send({ "Message": 'Thread not found!' })
            });
    },

    createThread(req, res, next) {
        const userName = req.body.author;
        const title = req.body.title;
        const content = req.body.content;

        try {
            assert(userName, 'Username must be provided!');
            assert(title, 'Title must be provided!');
            assert(content, 'Content must be provided!');
        } catch (err) {
            next(res.status(422).send(err.message));
        }

        User.findOne({ userName: userName })
            .then(user => {
                if (user === null) {
                    next(res.status(204).send({ "Message": 'User does not exist!' }))
                } else {
                    const thread = new Thread({
                        "author": user,
                        "title": title,
                        "content": content
                    });
                    user.threads.push(thread);
                    Promise.all([user.save(), thread.save()])
                        .then(() => {
                            next(res.status(200).send({ "Message": 'Thread succesfully placed!' }))
                        })
                        .catch(() => {
                            next(res.status(204).send({ "Message": 'Thread not placed!' }))
                        });
                }
            })
            .catch(error => {
                res.status(error.status).send({ Message: error.message })
            });
    },

    updateThread(req, res, next) {
        const id = req.params.id;
        const userName = req.body.userName;
        const password = req.body.password;
        const newContent = req.body.content;

        try {
            assert(userName, 'New content must be provided!');
            assert(password, 'New content must be provided!');
            assert(newContent, 'New content must be provided!');
            assert(id, 'New content must be provided!');
        } catch (err) {
            next(res.status(422).send(err.message));
        }

        validator.validateUser(userName, password)
            .then(user => {
                Thread.findOne({_id: id})
                    .then((thread) => {
                        if (thread.author.toString() === user._id.toString()) {

                            thread.set('content', newContent);
                            thread.save()
                                .then(thread => {
                                    res.status(200).send({
                                        message: "Thread updated successfully!",
                                        content: newContent
                                    });
                                });
                        } else {
                            res.status(401).send({
                                message: "Unauthorized to delete this thread!"
                            });
                        }
                    })
                    .catch(() => {
                        res.status(404).send({
                            message: "Thread not found!"
                        })
                    });
            })
            .catch(error => {
                res.status(error.status).send({message: error.message});
            });
    },

    deleteThread(req, res, next) {
        const threadId = req.params.id;

        try {
            assert(threadId, 'Thread id must be provided!');
        } catch (err) {
            next(res.status(422).send(err.message));
        }

        Thread.findOne({_id: req.params.id})
            .then((thread) => {
                if (thread === undefined || thread === null) {
                    res.status(404).send({"Message": 'Thread not found!'})
                } else {
                    thread.delete();
                    res.status(200).send({"Message": 'Thread is succesfully deleted!'})
                }
            })
            .catch(() => {
                res.status(204).send({"Message": 'Thread is not deleted!'})
            })
    }
};
