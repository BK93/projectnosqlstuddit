const { session } = require('../neo4j_setup');

module.exports = {

    addFriendship(req, res, next){
        const params = {
            userName: req.body.userName,
            friendName: req.body.friendName
        };

        if (params.userName === undefined || params.friendName === undefined) {
            res.status(409).json({ "Error": "One or both fields are empty!" });
        }

        session.run('MATCH (u:User {userName: $userName}), (f:User {userName: $friendName}) return u', params)
            .then((result) => {
                if (!result.records[0]) {
                    res.status(409).json({ "Error": "User does not exist!" });
                } else {
                    session.run('MATCH (u:User {userName: $userName}), (f:User {userName: $friendName}) MERGE (u)-[:Friends]-(f)', params)
                        .then(() => {
                            res.status(200).json({ "Message": "Friendship is created!" });
                        })
                        .catch((error) => {
                            res.status(204).json({ "Error": "Friendship not created!" });
                        });
                }
            })
            .catch(() => {
                res.status(409).send({ "Message": 'Error has occured!' })
            });
    },

    deleteFriendship(req, res, next){
        const params = {
            userName: req.body.userName,
            friendName: req.body.friendName
        };

        if (params.userName === undefined || params.friendName === undefined) {
            res.status(409).json({ "Error": "One or both fields are empty!" });
        }

        session.run('MATCH (u:User {userName: $userName}), (f:User {userName: $friendName}) return u', params)
            .then((result) => {
                if (!result.records[0]) {
                    res.status(409).json({ "Error": "User does not exist1" });
                } else {
                    session.run('MATCH (u:User {userName: $userName})-[r:Friends]-(:User {userName: $friendName}) DELETE r', params)
                        .then(() => {
                            res.status(200).json({ "Message": "Friendship is removed!" })
                        })
                        .catch((error) => {
                            res.status(204).json({ "Error": "Friendship not removed!" });
                        });
                }
            })
            .catch(() => {
                res.status(409).send({ "Message": 'Error has occured!' })
            });
    }
};
