const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
require('dotenv').config();

const neo4j = require('neo4j-driver').v1;
const neo = require('./neo4j_setup');

// All the routes:
const UserRoutes = require('./routes/userRoutes');
const FriendshipRoutes = require('./routes/friendshipRoutes');
const ThreadRoutes = require('./routes/threadRoutes');
const CommentRoutes = require('./routes/commentRoutes');
const VoteRoutes = require('./routes/voteRoutes');

// make mongoose use ES6 promises
mongoose.Promise = global.Promise;

// connect to mongodb
// mongoose.connect('mongodb://localhost/studdit', {useNewUrlParser: true});
//mongodb+srv://admin:<password>@studdit-2xsrs.gcp.mongodb.net/test?retryWrites=true&w=majority
// mongodb://admin:StudditAdmin1!@studdit-shard-00-00-2xsrs.gcp.mongodb.net:27017,studdit-shard-00-01-2xsrs.gcp.mongodb.net:27017,studdit-shard-00-02-2xsrs.gcp.mongodb.net:27017/test?ssl=true&replicaSet=Studdit-shard-0&authSource=admin&retryWrites=true&w=majority
mongoose.connect(process.env.MONGODB_CONNECTION, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    useCreateIndex: true
})
    .then(() => {}, err => {console.log(err)});

// connect to neo4j db
neo.driver = neo4j.driver(
    process.env.NEO4J_CONNECTION.toString(),
    neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASSWORD)
);

// on shutdown disconnect from neo4j db
process.on('exit', function() {
    neo.driver.close();
});

const app = express();

app.all('*', function(req, res, next){
    next()
});

app.use(bodyParser.json());

UserRoutes(app);
FriendshipRoutes(app);
ThreadRoutes(app);
CommentRoutes(app);
VoteRoutes(app);

module.exports = app;
