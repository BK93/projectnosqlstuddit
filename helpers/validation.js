const User = require('../models/user');

module.exports = {
    /**
     * Finds user with given username in database and
     * checks if given password is valid
     * @param {string} userName
     * @param {string} password
     * @returns {User} if resolved and object with
     * errormessage if rejected
     */
    validateUser(userName, password) {
        return new Promise((resolve, reject) => {
            let error = "";
            User.findOne({ userName: userName })
                .then(user => {
                    if (user === null) {
                        error = {
                            message: `Username ${userName} not found.`,
                            status: 404
                        };
                        reject(error);
                    } else {
                        if (user.password === password) {
                            resolve(user);
                        } else {
                            error = {
                                message: "Unauthorized: password is invalid.",
                                status: 401
                            };
                            reject(error);
                        }
                    }
                })
                .catch((error) => {
                    reject(error)
                });
        });
    },

    /**
     * Finds user with given username in database
     * @param {string} userName
     * @returns {User} if resolved and object with
     * errormessage if rejected
     */
    findUser(userName) {
        return new Promise((resolve, reject) => {
            let error = "";
            User.findOne({ userName: userName })
                .then(user => {
                    if (user === null) {
                        error = {
                            message: `Username ${userName} not found.`,
                            status: 404
                        };
                        reject(error);
                    } else {
                        resolve(user);
                    }
                });
        });
    }
};
