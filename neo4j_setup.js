const neo4j = require('neo4j-driver').v1;
require('dotenv').config();

const driver = neo4j.driver(
    process.env.NEO4J_CONNECTION.toString(),
    neo4j.auth.basic(process.env.NEO4J_USER, process.env.NEO4J_PASSWORD)
    );

const session = driver.session();

module.exports = {
    neo4j,
    session
};
